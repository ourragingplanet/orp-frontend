var setProperties = {
  'drainage-assets': {
    name: 'Drainage Asset',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/drainage-assets/marker-icon.png',
        iconSize: [15, 15],
        iconAnchor: [10, 10],
        tooltipAnchor: [10, 10]
      },
      warn: null
    }
  },
  'belfast-trees': {
    name: 'Belfast Trees',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/belfast-trees/marker-icon.png',
        iconRetinaUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/belfast-trees/marker-icon.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/belfast-trees/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        tooltipAnchor: [16, -28],
        shadowSize: [41, 41]
      }
    },
    formatter: {
      normal: {
        name: function (data) {
          // return data.properties.CurrentUse;
                        return "Belfast tree";
        },
        description: function (data) {
          // var response = data.properties.Extent + '\r' + data.properties.Address;
          var response = "This is the location of a tree maybe?";
                    // response += '. \rCurrently a ' + data.properties.CurrentUse;
                    // response += '. \rUsed to be a ' + data.properties.FormerUse;
          return response;
        }
      }
    },
      // warn: {
        // iconUrl: '/images/leaflet/listed-buildings/marker-icon.png',
        // iconRetinaUrl: '/images/leaflet/listed-buildings/marker-icon.png',
        // shadowUrl: '/images/leaflet/listed-buildings/marker-shadow.png',
        // iconSize: [25, 41],
        // iconAnchor: [12, 41],
        // popupAnchor: [1, -34],
        // tooltipAnchor: [16, -28],
        // shadowSize: [41, 41]
      // },
    image: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/listed-buildings/marker-icon.jpg'

},
  'listed-buildings': {
    name: 'Listed Building',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/listed-buildings/marker-icon.png',
        iconRetinaUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/listed-buildings/marker-icon-2x.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/listed-buildings/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        tooltipAnchor: [16, -28],
        shadowSize: [41, 41]
      }
    },
    formatter: {
      normal: {
        name: function (data) {
          return data.properties.CurrentUse;
        },
        description: function (data) {
          var response = data.properties.Extent + '\r' + data.properties.Address;
                    response += '. \rCurrently a ' + data.properties.CurrentUse;
                    response += '. \rUsed to be a ' + data.properties.FormerUse;
          return response;
        }
      }
    },
      warn: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/listed-buildings/marker-icon.png',
        iconRetinaUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/listed-buildings/marker-icon.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/listed-buildings/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        tooltipAnchor: [16, -28],
        shadowSize: [41, 41]
      },
    image: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/listed-buildings/marker-icon.jpg'

},
  'saudi-arabia-highways': {
    name: 'Highways',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/normal/marker-icon.png',
        iconRetinaUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/normal/marker-icon-2x.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/normal/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        tooltipAnchor: [16, -28],
        shadowSize: [41, 41]
      },
      warn: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/alert/marker-icon.png',
        iconRetinaUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/alert/marker-icon-2x.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/alert/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        tooltipAnchor: [16, -28],
        shadowSize: [41, 41]
      }
    },
    image: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/normal/bus-stop.jpg'
  },
  'bus-stop': {
    name: 'Bus Stop',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/normal/marker-icon.png',
        iconRetinaUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/normal/marker-icon-2x.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/normal/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        tooltipAnchor: [16, -28],
        shadowSize: [41, 41]
      },
      warn: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/alert/marker-icon.png',
        iconRetinaUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/alert/marker-icon-2x.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/alert/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        tooltipAnchor: [16, -28],
        shadowSize: [41, 41]
      }
    },
    image: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/normal/bus-stop.jpg'
  },
  'community-centre': {
    name: 'Community Centre',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/community-centre/marker-icon.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/community-centre/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      },
      warn: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/community-centre/marker-icon-alert.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/community-centre/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      }
    },
    formatter: {
      normal: {
        name: function (data) {
          return data.NAME;
        },
        description: function (data) {
          return 'Community centre on ' + data.ADDRESS;
        }
      },
      warn: null
    }
  },
  'railway-station': {
    name: 'Railway Station',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/railway-station/marker-icon.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/railway-station/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      },
      warn: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/railway-station/marker-icon-alert.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/railway-station/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      }
    },
    formatter: {
      normal: {
        name: function (data) {
          var props = data.properties;
          return props.Station;
        },
        description: function (data) {
          return 'Station for local trains', data;
        }
      },
      warn: null
    },
    image: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/railway-station/railway-station.jpg'
  },
  'school-locations': {
    name: 'School',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/my-school/marker-icon.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/my-school/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      },
      warn: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/my-school/marker-icon-alert.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/my-school/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      }
    },
    formatter: {
      normal: {
        name: function (data) {
          return data.Institution_Name;
        },
        description: function (data) {
          var response = data.Institution_Type.replace('(non-grammar) ', '');
          if (data.Current_Approved_Enrolment) {
            response += ' with ' + data.Current_Approved_Enrolment + ' students';
            if (data.Number_of_FTE_Staff) {
              response += ' and around ' + Math.round(data.Number_of_FTE_Staff) + ' staff';
            }
          }
          return response;
        }
      },
      warn: null
    },
    image: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/my-school/my-school.jpg'
  },
  'street-light': {
    name: 'Street Light',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/lighting/marker-icon.png',
        iconSize: [25, 25],
        iconAnchor: [12, 12],
        tooltipAnchor: [12, 12]
      },
      warn: null
    }
  },
  'park': {
    name: 'Park',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/park/marker-icon.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/park/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      },
      warn: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/park/marker-icon-alert.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/park/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      }
    }
  },
  'sports-facility': {
    name: 'Sports Facility',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/sports-facility/marker-icon.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/sports-facility/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      },
      warn: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/sports-facility/marker-icon-alert.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/sports-facility/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      }
    },
    formatter: {
      normal: {
        name: function (data) {
          return data.VENUE_NAME;
        },
        description: function (data) {
          var contains = '';
          for (var key in data) {
            if (data[key] === 'Yes') {
              contains += ', ' + key;
            }
          }
          contains = contains.replace('_', ' ').substr(1).toLowerCase();
          contains = 'Contains sports facilities: ' + contains;
          return contains;
        }
      },
      warn: null
    },
    image: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/sports-facility/sports-facility.jpg'
  },
  'waste-site': {
    name: 'Waste Site',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/waste-site/marker-icon.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/waste-site/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      },
      warn: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/waste-site/marker-icon-alert.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/waste-site/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      }
    }
  },
  'industrial-heritage': {
    name: 'Industrial Heritage',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/industrial-heritage/marker-icon.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/industrial-heritage/marker-shadow.png',
        iconSize: [24, 41],
        iconAnchor: [24, 41],
        shadowAnchor: [20, 34],
        tooltipAnchor: [12, 41],
        shadowSize: [29, 33]
      },
      warn: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/industrial-heritage/marker-icon-alert.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/industrial-heritage/marker-shadow.png',
        iconSize: [24, 41],
        iconAnchor: [24, 41],
        shadowAnchor: [20, 34],
        tooltipAnchor: [12, 41],
        shadowSize: [29, 33]
      }
    },
    formatter: {
      normal: {
        name: function (data) {
          var props = data.properties;
          return props.X_TYPE + ' at ' + props.TD;
        },
        description: function (data) {
          var response;
          var props = data.properties;
          var location = props.LOCATION;
          if (!location) {
            location = props.TD;
          }
          response = 'Part of our industrial history, this ' + props.X_TYPE + ' is located at ' + location;
          return response;
        }
      },
      warn: null
    }
  },
  'focal-point': {
    name: 'Focal Point',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/focal-point/marker-icon.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/focal-point/marker-shadow.png',
        iconSize: [96, 164],
        iconAnchor: [42, 164],
        shadowAnchor: [40, 136],
        tooltipAnchor: [48, 164],
        shadowSize: [118, 132]
      }
    },
    image: process.env.VUE_APP_PUBLIC_PATH + '/images/focal_point.png'
  },
  'my-school': {
    name: 'My School',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/my-school/marker-icon.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/my-school/marker-shadow.png',
        iconSize: [96, 164],
        iconAnchor: [42, 164],
        shadowAnchor: [40, 136],
        tooltipAnchor: [48, 164],
        shadowSize: [118, 132]
      },
      warn: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/my-school/marker-icon.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/my-school/marker-shadow.png',
        iconSize: [96, 164],
        iconAnchor: [42, 164],
        shadowAnchor: [40, 136],
        tooltipAnchor: [48, 164],
        shadowSize: [118, 132]
      }
    },
    image: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/my-school/my-school.jpg'
  },
  'unexplored-cultural-heritage': {
    name: 'Unexplored Heritage',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/unexplored-cultural-heritage/marker-icon.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/unexplored-cultural-heritage/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      },
      warn: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/unexplored-cultural-heritage/marker-icon-alert.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/unexplored-cultural-heritage/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      }
    }
  },
  'regional-waters': {
    name: 'Regional Waters',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/regional-waters/marker-icon.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/regional-waters/marker-shadow.png',
        iconSize: [24, 41],
        iconAnchor: [24, 41],
        shadowAnchor: [20, 34],
        tooltipAnchor: [12, 41],
        shadowSize: [29, 33]
      },
      warn: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/regional-waters/marker-icon-alert.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/regional-waters/marker-shadow.png',
        iconSize: [24, 41],
        iconAnchor: [24, 41],
        shadowAnchor: [20, 34],
        tooltipAnchor: [12, 41],
        shadowSize: [29, 33]
      }
    },
  formatter: {
    normal: {
      name: function (data) {
        return data.properties.name;
      },
      description: function (data) {
          return data.properties.description;
      }
    },
    warn: null
  },
  image: process.env.VUE_APP_PUBLIC_PATH + '/images/focal_point.png'
},
  'saudi-arabia-railway-station': {
    name: 'Saudi Arabia, Riyadh Railway stations',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/railway-station/marker-icon.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/railway-station/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      },
      warn: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/railway-station/marker-icon-alert.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/railway-station/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      }
    },
    image: process.env.VUE_APP_PUBLIC_PATH + '/images/railway_saudi_arabia.jpg'
  },
  'saudi-arabia-riyadh-park': {
    name: 'Saudi Arabia Riyadh Parks',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/park/marker-icon.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/park/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      },
      warn: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/park/marker-icon-alert.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/park/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      }
    },  
  formatter: {
    normal: {
      name: function (data) {
        var props = data.properties;
        return data.properties.name;
      },
      description: function (data) {
          return data.properties.description;
      }
    },
    warn: null
  },
  image: process.env.VUE_APP_PUBLIC_PATH + '/images/parks2_saudi_arabia.jpg'
},
  'saudi-arabia-dhahran-park': {
    name: 'Saudi Arabia Dhahran Parks',
    icon: {
      normal: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/park/marker-icon.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/park/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      },
      warn: {
        iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/park/marker-icon-alert.png',
        shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/park/marker-shadow.png',
        iconSize: [48, 82],
        iconAnchor: [24, 82],
        shadowAnchor: [15, 68],
        tooltipAnchor: [24, 82],
        shadowSize: [59, 66]
      }
    },  
  formatter: {
    normal: {
      name: function (data) {
        var props = data.properties;
        return data.properties.name;
      },
      description: function (data) {
          return data.properties.description;
      }
    },
    warn: null
  },
  image: process.env.VUE_APP_PUBLIC_PATH + '/images/parks_saudi_arabia.jpg'
},
'saudi-arabia-hospitals': {
  name: 'Saudi Arabia Riyadh Hospitals',
  icon: {
    normal: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/community-centre/marker-icon.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/community-centre/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    },
    warn: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/community-centre/marker-icon-alert.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/community-centre/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    }
  },  
formatter: {
  normal: {
    name: function (data) {
      var props = data.properties;
      return data.properties.name;
    },
    description: function (data) {
        return data.properties.description;
    }
  },
  warn: null
}},
'saudi-arabia-riyadh-highways': {
  name: 'Saudi Arabia Riyadh Highways',
  icon: {
    normal: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/community-centre/marker-icon.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/community-centre/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    },
    warn: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/community-centre/marker-icon-alert.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/community-centre/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    }
  },  
formatter: {
  normal: {
    name: function (data) {
      var props = data.properties;
      return data.properties.name;
    },
    description: function (data) {
        return data.properties.description;
    }
  },
  warn: null
}
},
'saudi-arabia-dhahran-highways': {
  name: 'Saudi Arabia Dhahran Highways',
  icon: {
    normal: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/community-centre/marker-icon.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/community-centre/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    },
    warn: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/community-centre/marker-icon-alert.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/community-centre/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    }
  },  
formatter: {
  normal: {
    name: function (data) {
      var props = data.properties;
      return data.properties.name;
    },
    description: function (data) {
        return data.properties.description;
    }
  },
  warn: null
},
image: process.env.VUE_APP_PUBLIC_PATH + '/images/traffic_lights.jpg'
},
'saudi-arabia-museums': {
  name: 'Saudi Arabia Riyadh Museums',
  icon: {
    normal: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/unexplored-cultural-heritage/marker-icon.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/unexplored-cultural-heritage/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    },
    warn: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/unexplored-cultural-heritage/marker-icon-alert.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/unexplored-cultural-heritage/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    }
  },  
formatter: {
  normal: {
    name: function (data) {
      var props = data.properties;
      return data.properties.name;
    },
    description: function (data) {
        return data.properties.description;
    }
  },
  warn: null
},
image: process.env.VUE_APP_PUBLIC_PATH + '/images/museum_saudi_arabia.jpg'
},
'saudi-arabia-public-transport-stop-stations': {
  name: 'Public Transport Stop Stations',
  icon: {
    normal: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/railway-station/marker-icon.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/railway-station/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    },
    warn: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/railway-station/marker-icon-alert.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/railway-station/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    }
  },  
formatter: {
  normal: {
    name: function (data) {
      var props = data.properties;
      return data.properties.name;
    },
    description: function (data) {
        return data.properties.description;
    }
  },
  warn: null
},
image: process.env.VUE_APP_PUBLIC_PATH + '/images/metro_riyadh.jpg'
},
'saudi-arabia-schools': {
  name: 'Schools',
  icon: {
    normal: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/my-school/marker-icon.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/my-school/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    },
    warn: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/my-school/marker-icon-alert.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/my-school/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    }
  },  
formatter: {
  normal: {
    name: function (data) {
      var props = data.properties;
      return data.properties.name;
    },
    description: function (data) {
        return data.properties.description;
    }
  },
  warn: null
}},
'saudi-arabia-parking': {
  name: 'Parkings',
  icon: {
    normal: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/alert/marker-icon.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/alert/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    },
    warn: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/alert/marker-icon-alert.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/alert/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    }
  },  
formatter: {
  normal: {
    name: function (data) {
      var props = data.properties;
      return data.properties.name;
    },
    description: function (data) {
        return data.properties.description;
    }
  },
  warn: null
},
image: process.env.VUE_APP_PUBLIC_PATH + '/images/parking_saudi_arabia.jpg'
},
'DEFAULT': {
  name: 'DEFAULT',
  icon: {
    normal: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/alert/marker-icon.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/alert/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    },
    warn: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/alert/marker-icon-alert.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/alert/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    }
  },  
formatter: {
  normal: {
    name: function (data) {
      var props = data.properties;
      return data.properties.name;
    },
    description: function (data) {
        return data.properties.description;
    }
  },
  warn: null
}
},
'saudi-arabia-place-of-worship': {
  name: 'Riyadh Place Of Worship',
  icon: {
    normal: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/religious-generic/marker-icon.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/religious-generic/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    },
    warn: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/religious-generic/marker-icon-alert.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/religious-generic/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    }
  },  
formatter: {
  normal: {
    name: function (data) {
      var props = data.properties;
      return data.properties.name;
    },
    description: function (data) {
        return data.properties.description;
    }
  },
  warn: null
},
image: process.env.VUE_APP_PUBLIC_PATH + '/images/place_of_worship.png'
},
'saudi-arabia-riyadh-place-of-worship': {
  name: 'Riyadh Place Of Worship Riyadh',
  icon: {
    normal: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/religious-generic/marker-icon.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/religious-generic/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    },
    warn: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/religious-generic/marker-icon-alert.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/religious-generic/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    }
  },  
formatter: {
  normal: {
    name: function (data) {
      var props = data.properties;
      return data.properties.name;
    },
    description: function (data) {
        return data.properties.description;
    }
  },
  warn: null
},
image: process.env.VUE_APP_PUBLIC_PATH + '/images/place_of_worship.png'
},
'saudi-arabia-dhahran-place-of-worship': {
  name: 'Dhahran Place Of Worship Riyadh',
  icon: {
    normal: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/religious-generic/marker-icon.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/religious-generic/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    },
    warn: {
      iconUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/religious-generic/marker-icon-alert.png',
      shadowUrl: process.env.VUE_APP_PUBLIC_PATH + '/images/leaflet/religious-generic/marker-shadow.png',
      iconSize: [48, 82],
      iconAnchor: [24, 82],
      shadowAnchor: [15, 68],
      tooltipAnchor: [24, 82],
      shadowSize: [59, 66]
    }
  },  
formatter: {
  normal: {
    name: function (data) {
      var props = data.properties;
      return data.properties.name;
    },
    description: function (data) {
        return data.properties.description;
    }
  },
  warn: null
},
image: process.env.VUE_APP_PUBLIC_PATH + '/images/place_of_worship.png'
},
};

class FeatureRepository {
  setLogger(logger) {
    this.$log = logger;
  }

  has (set) {
    return set in setProperties;
  }

  get (set, prop, mode) {
    if (!setProperties[set]) {
      this.$log.error('Missing set: ' + set);
      set = 'saudi-arabia-riyadh-park';
    }
    var data = setProperties[set][prop];
    
    if (!data) {
      return data;
    }
   
    
    if (mode) {
      if (mode in data && data[mode] !== null) {
        return data[mode];
      } else {
        return data['normal'];
      }
    }

    return data;
  }
}

const featureRepository = new FeatureRepository();

export default featureRepository;
