import axios from 'axios';
import Vue from 'vue';
import Vuex from 'vuex';
import utils from '../utils';
var JsonapiClient = require('@holidayextras/jsonapi-client');

Vue.use(Vuex);

/** MIT Lintol (c) ODI, 2018 -> END */
if (process.env.NODE_ENV !== 'testing') {
    //var csrfToken = document.querySelector('meta[name="csrf-token"]').content;
    //axios.defaults.headers.common = {
    //  'X-Requested-With': 'XMLHttpRequest',
    //  'X-CSRF-TOKEN': csrfToken
    //};
    //axios.interceptors.response.use(
    //    response => { return response; },
    //    function (error) {
    //      console.log(error);
    //      /**
    //      if (error.response.status === 401) {
    //        window.location = process.env.LOGIN_URL;
    //      }
    //      */
    //    }
    //);
}

function carbonToJs(time) {
    var secs = new Date(Date.parse(time));
    //var secs = new Date(Date.parse(time.substr(0, time.indexOf('.')).replace(' ', 'T')));
    return secs;
}

function enrichCaseContext(caseContext) {
    return caseContext;
}

function enrichSimulation(simulation, state) {
    simulation.phenomenon = () => {
        if (simulation.phenomenon_id in state.phenomenons) {
            return state.phenomenons[simulation.phenomenon_id];
        }
    };
    simulation.caseContext = () => {
        if (state.currentCaseContext && simulation.case_context_id === state.currentCaseContext.id) {
            return state.currentCaseContext;
        }
        if (simulation.case_context_id in state.caseContexts) {
            return state.caseContexts[simulation.case_context_id];
        }
    };
    return simulation;
}

var apiPrefix = process.env.MOCK ?
    process.env.MOCK_API_PREFIX :
    process.env.VUE_APP_API_PREFIX

const ACTION_NONE = 'action:none';
const ACTION_PENDING = 'action:pending';
const ACTION_REQUESTED = 'action:requested';

var client;
// We don't have an easy of adding a non-static header
const dynamicHeader = utils.getObjectWithEnumerableGetter(
    'X-XSRF-TOKEN',
    () => {
        const csrfCookie = document.cookie
            .split('; ')
            .find(row => row.startsWith('XSRF-TOKEN'));
        if (csrfCookie) {
            return decodeURIComponent(csrfCookie.split('=')[1]);
        }
        return undefined;
    }
)
client = new JsonapiClient(apiPrefix, {
    header: dynamicHeader
});
/* END */

const store = new Vuex.Store({
    state: {
        alerts: [],
        rtl: false,
        locale: 'en_GB',
        currentSimulationState: ACTION_NONE,
        currentSimulation: undefined,
        currentSimulationStatistics: undefined,
        currentFeatures: undefined,
        currentInformationals: undefined,
        currentDatasets: {},
        currentCaseContextState: ACTION_NONE,
        currentCaseContext: undefined,
        currentWidthOfMapInKm: 0,
        currentCenter: undefined,
        focusedSimulation: undefined,
        resultFeatures: [],
        cachedResultFeatures: {
            windows: [],
            dataSets: {}
        },
        affiliation: null,
        combinations: [],
        caseContexts: [],
        caseContextsState: ACTION_NONE,
        simulations: [],
        simulationsState: ACTION_NONE,
        currentChallenges: [],
        activeChallenge: null,
        activeInformational: null,
        missingFile: false,
        simulationObjects: [],
        phenomenons: [],
        phenomenonsState: ACTION_NONE,
        user: undefined,
        users: undefined,
        time: new Date(Date.now()),
        showMapDialog: false,
        fileUrl: undefined,
        mappingSystem: 'Leaflet',
        isNewsfeedActive: false,
        isTaskworkActive: false,
        sidebar: {
            content: null,
            image: null,
            icon: null,
            type: null,
            options: null,
            active: false,
            challengeType: null,
            isFullscreen: false
        },
        timelinePoints: {
            newsTimelinePoints: null,
            taskTimelinePoints: null
        }
    },
    mutations: {
        removeSimulation(state, simulation) {
            state.simulationObjects = state.simulationObjects.filter(s => s._base.id === simulation.id);
            state.simulations = state.simulations.filter(s => s.id !== simulation.id);
        },
        addAlert(state, data) {
            state.alerts.push({
                type: data[0],
                content: data[1]
            });
        },
        hideSidebar(state) {
            state.sidebar.active = false;
        },
        setSidebarContent(state, data) {
            state.sidebar.image = null;
            state.sidebar.active = true;
            state.sidebar.type = 'popover';
            state.sidebar.content = data.content;
            if (data.image) {
                state.sidebar.image = data.image;
            }
            state.sidebar.icon = data.icon;
        },
        setCurrentWidthOfMapInKm(state, currentWidthOfMapInKm) {
            state.currentWidthOfMapInKm = currentWidthOfMapInKm;
        },
        setCurrentChallenges(state, currentChallenges) {
            state.currentChallenges = currentChallenges;
        },
        setActiveChallenge(state, activeChallenge) {
            state.activeChallenge = activeChallenge;
        },
        setTimelinePoints(state, timelinePoints) {
            state.timelinePoints.newsTimelinePoints = timelinePoints.newsTimelinePoints;
            state.timelinePoints.taskTimelinePoints = timelinePoints.taskTimelinePoints;
        },
        setActiveInformational(state, activeInformational) {
            state.activeInformational = activeInformational;
        },
        setCurrentSimulationState(state, currentSimulationState) {
            state.currentSimulationState = currentSimulationState;
        },
        setCurrentCaseContextState(state, currentCaseContextState) {
            state.currentCaseContextState = currentCaseContextState;
        },
        setSimulationsState(state, simulationsState) {
            state.simulationsState = simulationsState;
        },
        setCaseContextsState(state, caseContextsState) {
            state.caseContextsState = caseContextsState;
        },
        setPhenomenonsState(state, phenomenonsState) {
            state.phenomenonsState = phenomenonsState;
        },
        setCombinations(state, combinations) {
            state.combinations = combinations.reduce(function(cs, combination) {
                combination.id = combination._base.id;
                cs[combination.id] = combination;
                return cs;
            }, {});
        },
        setCaseContexts(state, caseContexts) {
            state.caseContexts = caseContexts.reduce(function(ccs, caseContext) {
                ccs[caseContext.id] = caseContext;
                return ccs;
            }, {});
        },
        setCurrentInformationals(state, informationals) {
            state.currentInformationals = informationals;
        },
        setCurrentFeatures(state, features) {
            state.currentFeatures = features;
        },
        setCurrentSimulationStatistics(state, currentSimulationStatistics) {
            state.currentSimulationStatistics = currentSimulationStatistics;
        },
        setCurrentSimulation(state, simulation) {
            state.currentFeatures = undefined;
            state.resultFeatures = [];
            state.cachedResultFeatures = {
                windows: [],
                dataSets: {}
            };
            state.currentInformationals = undefined;
            state.currentDatasets = {};
            state.currentSimulation = enrichSimulation(simulation, state);
        },
        setCurrentCaseContext(state, caseContext) {
            state.currentCaseContext = enrichCaseContext(caseContext, state);
        },
        setResultFeatures(state, resultFeatures) {
            state.resultFeatures = resultFeatures;
        },
        setCachedResultFeaturesStart(state, start) {
            state.cachedResultFeatures.start = start;
        },
        setCachedResultFeaturesEnd(state, end) {
            state.cachedResultFeatures.end = end;
        },
        setSimulations(state, simulations) {
            state.simulations = simulations.map(function(simulation) {
                return enrichSimulation(simulation, state);
            });
        },
        setAffiliation(state, affiliation) {
            state.affiliation = affiliation;
        },
        setUser(state, user) {
            state.user = user;
        },
        setTime(state, time) {
            state.time = time;
        },
        setCurrentCenter(state, center) {
            state.currentCenter = center;
        },
        setFocusedSimulation(state, focusedSimulation) {
            state.focusedSimulation = focusedSimulation;
        },
        setPhenomenons(state, phenomenons) {
            state.phenomenons = phenomenons.reduce(function(phs, phenomenon) {
                phs[phenomenon.id] = phenomenon;
                return phs;
            }, {});
        },
        setFsSidebar(state) {
            state.sidebar.isFullscreen = !state.sidebar.isFullscreen
        },
        setUsers(state, users) {
            state.users = users
        },
        toggleMapDialog(state) {
            state.showMapDialog = !state.showMapDialog;
        },
        addDataset(state, { simulationId, dataset }) {
            if (state.currentSimulation && state.currentSimulation.id == simulationId) {
                state.currentDatasets[dataset.id] = dataset;
            }
        },
        addDataSetToCachedResultFeatures(state, { offset, features }) {
            state.cachedResultFeatures.dataSets[offset] = features;
            if (state.cachedResultFeatures.start !== undefined && offset < state.cachedResultFeatures.start) {
                state.cachedResultFeatures.start = offset;
            }
            if (state.cachedResultFeatures.end !== undefined && offset > state.cachedResultFeatures.end) {
                state.cachedResultFeatures.end = offset;
            }
        },
        addWindowToCachedResultFeatures(state, newWin) {
            state.cachedResultFeatures.windows.push(newWin);
        },
        addArcToCachedResultFeatures(state, { offset, arc }) {
            state.cachedResultFeatures.dataSets[offset].arc = arc;
        },
        addFeatureArc(state, { simulationId, arcId, arc }) {
            if (state.currentSimulation && state.currentSimulation.id == simulationId) {
                state.currentFeatureArcs[arcId] = arc;
            }
        },
        setLocale(state, locale) {
            state.locale = locale;
        },
        SET_RTL(state, rtl) {
            localStorage.setItem('rtl', rtl);
            state.rtl = rtl;
        },
        initialiseStore(state) {
            if (state.rtl)
                state.rtl = true;
            else
                state.rtl = false;
        },
    },
    getters: {
        globalRiskByTimeFn(context) {
            let timeOffsets = [];
            let max = undefined;
            let min = undefined;
            if (context.currentSimulationStatistics && context.currentSimulationStatistics.timeOffsets) {
                timeOffsets = Object.entries(context.currentSimulationStatistics.timeOffsets).map(([key, value]) => {
                    if ('count-0.5' in value) {
                        if (max === undefined || max < value['count-0.5']) {
                            max = value['count-0.5'];
                        }
                        if (min === undefined || min > value['count-0.5']) {
                            min = value['count-0.5'];
                        }
                        return [key * 1000, value['count-0.5']];
                    }
                }).filter(v => v).sort((a, b) => (a[1] - b[1]));
            }
            const begins = Number(carbonToJs(context.currentSimulation.begins));
            return (time) => {
                let diff = 0;
                const scale = x => (x - min) / (max - min);
                if (begins && timeOffsets.length > 0) {
                    diff = (Number(time) - begins);
                } else {
                    return [0, false];
                }
                if (diff <= timeOffsets[0][0]) {
                    return [scale(timeOffsets[0][1]), false];
                }
                if (diff >= timeOffsets[timeOffsets.length - 1][0]) {
                    return [scale(timeOffsets[timeOffsets.length - 1][1]), false];
                }
                let lastOffset = timeOffsets[0];
                return timeOffsets.reduce((found, offset) => {
                    if (found === undefined && offset[0] > diff) {
                        const interp = lastOffset[1] + (offset[1] - lastOffset[1]) * (diff - lastOffset[0]) / (offset[0] - lastOffset[0]);
                        return [scale(interp), true];
                    } else {
                        return found;
                    }
                }, undefined);
            };
        },
        timeOffset(context) {
            var diff = 0;
            if (context.currentSimulation) {
                var date = context.currentSimulation.begins;
                date = carbonToJs(date);
                diff = (Number(context.time) - Number(date));
            }
            return diff;
        }
    },
    actions: {
        async updateWidthOfMapInKm({ commit }, widthOfMapInKm) {
            commit('setCurrentWidthOfMapInKm', widthOfMapInKm);
        },

        async createCaseContext({ commit }, { center }, length) {
            if (!length) {
                length = 5;
            }
            var caseContext = client.create('case-contexts');
            caseContext.set('center', center);
            const now = new Date();
            const then = new Date(now.getTime() + 60 * 60 * 1000 * length);
            // caseContext.set('begins', now);
            // caseContext.set('ends', then);
            const res = await caseContext.sync();
            return res;
        },

        async deleteSimulation({ commit }, simulation) {
            var simObject = await client.get('simulations', simulation.id);
            try {
                await simObject.delete();
                commit('removeSimulation', simulation);
            } catch (err) {
                this.$log.debug(err);
            }
        },

        async createSimulation({ commit }, { combination, caseContext }) {
            var simulation = client.create('simulations');
            simulation.relationships('combination').set(combination);
            simulation.relationships('caseContext').set(caseContext);
            simulation.set('begins', caseContext.get('begins'));
            simulation.set('ends', caseContext.get('ends'));
            await simulation.sync();
            var simulationRun = client.create('simulations/' + simulation._base.id + '/run');
            simulationRun.sync();
            return simulation;
        },

        getTaskFile({ state }) {
            var challengeID = state.activeChallenge.id;
            axios({
                url: '/api/getTaskFile/' + challengeID,
                method: 'GET',
            }).then((response) => {
                var fileURL = response.data
                var fileLink = document.createElement('a');
                fileLink.href = fileURL;
                fileLink.setAttribute('download', 'file.pdf');
                document.body.appendChild(fileLink);
                fileLink.click();
            }).catch((error) => {
                state.missingFile = true;
            });
        },

        getNewsFile({ state }) {
            var informationalID = state.activeInformational.id;
            axios({
                url: '/api/getNewsFile/' + informationalID,
                method: 'GET',
            }).then((response) => {
                var fileURL = response.data
                var fileLink = document.createElement('a');
                fileLink.href = fileURL;
                fileLink.setAttribute('download', 'file.pdf');
                document.body.appendChild(fileLink);
                fileLink.click();
            }).catch((error) => {
                state.missingFile = true;
            });
        },

        cloneSimulation(state, payload) {
            console.log(payload.id)
            axios.post('/api/v1/cloneSimulation', payload)
                .then((response) => {
                    console.log(response)
                }).catch((error) => {
                    console.log(error)
                });
        },

        getUsers({ commit }) {
            axios.get(apiPrefix + '/users')
                .then((response) => {
                    commit('setUsers', response.data)
                }).catch((error) => {
                    console.log(error)
                });
        },

        setLocale({ commit }, locale) {
            commit('setLocale', locale);
        },

        recenterLocation({ commit }, center) {
            commit('setCurrentCenter', center);
        },

        login({ state }) {
            window.location = `/${state.locale}/login`;
        },

        fetchCurrentUser({ commit, dispatch }) {
            return client.get('users', 'current', {
                    fields: { users: 'defaultLocation' },
                    includes: 'affiliation'
                })
                .then(data => {
                    commit('setUser', data.toJSON());
                    if (data.affiliation) {
                        commit('setAffiliation', data.affiliation.toJSON());
                    }
                })
                .catch(err => {
                    // This is how jsonapi-client passes a 401
                    if (err.message === '401 Unauthorized') {
                        try {
                            dispatch('login');
                        } catch (err) {
                            this.$log.debug(err);
                        }
                    }
                    return true;
                });
        },

        focusOnSimulation({ commit }, simulation) {
            commit('setFocusedSimulation', simulation);
        },

        fetchChallenges({ commit }, { simulationId }) {
            return axios.get(apiPrefix + '/simulations/' + simulationId + '/challenges').then(response => {
                    const challenges = response.data.data.map(challenge => ({ id: challenge.id, ...challenge.attributes }));
                    commit('setCurrentChallenges', challenges);
                    return challenges;
                })
                .catch(response => {
                    // this.$log.debug('Could not get data task Data');
                });
        },

        activateChallenge({ commit, state }, challenge) {
            if (Number.isInteger(challenge)) {
                challenge = state.currentChallenges[challenge];
                state.sidebar.type = 'task';
                state.sidebar.active = true;
                state.sidebar.content = (
                    '<h1 class="task-title" v-translate>Task</h1>' +
                    '<div class="task-content">' +
                    '    <h5>' + challenge.text + '</h5>' +
                    '    <p><h7>' + challenge.subtext + '</h7></p>' +
                    '</div>');
                state.sidebar.options = challenge.options;
                state.sidebar.challengeType = challenge.challenge_type;
            }
            commit('setActiveChallenge', challenge);
            if (challenge && challenge.time) {
                commit('setTime', carbonToJs(challenge.time));
            }
            return challenge;
        },
        activateInformational({ commit, state }, informational) {
            if (Number.isInteger(informational)) {
                informational = state.currentInformationals[informational];
                state.sidebar.type = 'news';
                state.sidebar.active = true;
                state.sidebar.content = (
                    '<h1 class="news-title" v-translate>News</h1>' +
                    '<div class="news-content">' +
                    '    <h5>' + informational.text + '</h5>' +
                    '    <p><h7>' + informational.subtext + '</h7></p>' +
                    '</div>');
            }
            commit('setActiveInformational', informational);
            if (informational && informational.time) {
                commit('setTime', carbonToJs(informational.time));
            }
            return informational;
        },

        fetchResultFeatures(context) {
            var state = context.state;
            var features = state.currentFeatures;
            var dispatch = dispatch;
            var cachedResultFeatures = state.cachedResultFeatures;
            var endTime = carbonToJs(state.currentSimulation.ends);
            var beginTime = carbonToJs(state.currentSimulation.begins);
            var timeOffset = context.getters.timeOffset;
            var data = null;

            if (cachedResultFeatures) {
                //this.$log.debug('Checking cached features');
                // 3 if statements to catch simulations between phases
                // first statement is when the simulation starts and the offset is 0(roughly) and the start isn't defined
                if (cachedResultFeatures.start !== undefined && timeOffset / 1000 <= cachedResultFeatures.start) {
                    //this.$log.debug('Using start');
                    //generates data from the datasets in the cache
                    data = [cachedResultFeatures.dataSets[cachedResultFeatures.start]];
                    //the second if catches the simulation between 2 phases
                } else if (cachedResultFeatures.end !== undefined && timeOffset / 1000 >= cachedResultFeatures.end) {
                    //this.$log.debug('Using end');
                    // generates data from the cached dataset
                    data = [cachedResultFeatures.dataSets[cachedResultFeatures.end]];
                } else {
                    //last statement catches the simulation at the next phase
                    //this.$log.debug('Checking windows for ' + this.timeOffset);
                    // gets the variable value using the function declared?
                    var win = cachedResultFeatures.windows.find(function(win) {
                        // sets the value of the variable as the difference between the 1st and 2nd window values (simulation phases?)
                        return (win[0] <= timeOffset / 1000 && win[1] >= timeOffset / 1000);
                    });
                    /*
                    var win = this.cachedResultFeatures.windows.reduce(function (acc, win) {
                      this.$log.debug('...' + win[0] + ' to ' + win[1]);
                      if (acc || (win[0] <= timeOffset && win[1] >= timeOffset)) {
                        return acc;
                      };
                    });
                    */
                    // if the window has data?
                    if (win) {
                        //adds values to the data which was null using the simulation phases at the current time 
                        data = [
                            cachedResultFeatures.dataSets[win[0]],
                            cachedResultFeatures.dataSets[win[1]]
                        ];
                        //this.$log.debug('Found window ' + win[0] + ' to ' + win[1] + ' for ' + this.timeOffset);
                    }
                }
                if (data) {
                    //if the data is not null then return the promise with the data
                    return context.commit('setResultFeatures', data);
                }
            }
            // context.dispatch('fetchCurrentFeatures');
        },
        cacheResultFeatures(context, features) {
            var state = context.state;
            var dispatch = context.dispatch;
            // var features = state.currentFeatures;

            var timeOffset = context.getters.timeOffset;
            var endTime = carbonToJs(state.currentSimulation.ends);
            var beginTime = carbonToJs(state.currentSimulation.begins);
            var promises = [];
            var offsets = [];
            const cachedResultFeatures = context.state.cachedResultFeatures;
            if (!features || features.length == 0) {
                return null;
            }

            /* We rely on features being either side of the timeOffset,
             * as that's the only time we should get here, unless the
             * timeOffset is off the ends of the simulation. If not, then
             * the simulation has a gap, or timeOffset has changed since
             * the currentFeatures updates (although that should be where
             * this is called from)
             */

            //if catches the current simulation phase
            if (timeOffset <= 0 || timeOffset / 1000 < features[0].time_offset) {
                offsets.push(features[0].time_offset);
                features[0].arc = [];
                context.commit('addDataSetToCachedResultFeatures', { offset: offsets[0], features: features[0] });
                context.commit('setCachedResultFeaturesStart', offsets[0]);
                //if to catch when the time is between phases
            } else if (timeOffset >= endTime - beginTime || timeOffset / 1000 > features[features.length - 1].time_offset) {
                offsets.push(features[0].time_offset);
                features[0].arc = [];
                context.commit('addDataSetToCachedResultFeatures', { offset: offsets[0], features: features[0] });
                context.commit('setCachedResultFeaturesEnd', offsets[0]);
                //if to catch the second phase (2nd features)
            } else if (features.length === 2) {
                features[0].arc = [];
                features[1].arc = [];

                offsets.push(features[0].time_offset);
                offsets.push(features[1].time_offset);
                context.commit('addDataSetToCachedResultFeatures', { offset: offsets[0], features: features[0] });
                context.commit('addDataSetToCachedResultFeatures', { offset: offsets[1], features: features[1] });

                var newWin = [
                    //populates the win variable with the offset values
                    offsets[0],
                    offsets[1]
                ];
                context.commit('addWindowToCachedResultFeatures', newWin);

                if (timeOffset < features[0].time_offset) {
                    offsets.push(features[0].time_offset);
                }
                //this.$log.debug(newWin);
            } else {
                return null;
            }

            //this.$log.debug(offsets);
            var promiseOffsets = [];
            offsets.forEach(function(offset) {
                if (cachedResultFeatures.dataSets[offset] ||
                    cachedResultFeatures.dataSets[offset].arc) {
                    promiseOffsets.push(offset);
                    promises.push(dispatch('fetchDataset', cachedResultFeatures.dataSets[offset].id));
                }
            });

            return Promise.all(promises).then(function(responses) {
                return responses.forEach(function(response, i) {
                    context.commit('addArcToCachedResultFeatures', { offset: promiseOffsets[i], arc: response.data });
                });
            }).then(() => {
                return context.dispatch('fetchResultFeatures');
            });
        },
        toggleMapDialog(context) {
            context.commit('toggleMapDialog');
        },
        fetchPhenomenons(context, { refresh }) {
            if (context.state.phenomenonsState === ACTION_NONE || refresh && context.state.phenomenonsState === ACTION_REQUESTED) {
                context.commit('setPhenomenonsState', ACTION_PENDING);
                return client.find('phenomenons').then(data => {
                    context.commit('setPhenomenons', data.map(p => p.toJSON()));
                    context.commit('setPhenomenonsState', ACTION_REQUESTED);
                }).catch(() => {
                    context.commit('addAlert', ['danger', "Couldn't get your phenomenons!"]);
                    context.commit('setPhenomenonsState', ACTION_NONE);
                });
            }
            return Promise.resolve();
        },
        fetchCaseContexts(context, { refresh }) {
            if (context.state.caseContextsState === ACTION_NONE || refresh && context.state.caseContextsState === ACTION_REQUESTED) {
                context.commit('setCaseContextsState', ACTION_PENDING);
                return client.find('case-contexts').then(data => {
                    context.commit('setCaseContexts', data.map(c => c.toJSONTree()));
                    context.commit('setCaseContextsState', ACTION_REQUESTED);
                }).catch(() => {
                    context.commit('addAlert', ['danger', "Couldn't get your case contexts!"]);
                    context.commit('setCaseContextsState', ACTION_NONE);
                });
            }
            return Promise.resolve();
        },
        fetchCombinations(context) {
            client.find('combinations', { include: ['phenomenon', 'numericalModel'] }).then(data => {
                context.commit('setCombinations', data);
            });
        },
        fetchSimulations(context, { refresh }) {
            if (context.state.simulationsState === ACTION_NONE || refresh && context.state.simulationsState === ACTION_REQUESTED) {
                context.commit('setSimulationsState', ACTION_PENDING);
                context.dispatch('fetchPhenomenons', { refresh: false }).then(() => {
                    return context.dispatch('fetchCaseContexts', { refresh: false });
                }).then(() => {
                    return client.find('simulations');
                }).then(data => {
                    context.commit('setSimulations', data.map(s => s.toJSON()));
                    context.commit('setSimulationsState', ACTION_REQUESTED);
                }).catch(() => {
                    context.commit('addAlert', ['danger', "Couldn't get your simulations!"]);
                    context.commit('setSimulationsState', ACTION_NONE);
                });
            }
            return Promise.resolve();
        },
        fetchCurrentInformationals(context) {
            var simulation = context.state.currentSimulation;
            if (simulation) {
                return client.find('simulations/' + simulation.id + '/informationals').then((data) => {
                    var informationals = data.map((f) => f.toJSON());
                    context.commit('setCurrentInformationals', informationals);
                    return informationals;
                });
            }
            return Promise.resolve();
        },
        fetchCurrentFeatures(context) {
            var simulation = context.state.currentSimulation;
            if (simulation) {
                var timeOffset = context.getters.timeOffset;
                return client.find('simulations/' + simulation.id + '/features?time_offset=' + Math.round(timeOffset / 1000)).then((data) => {
                    var features = data.map((f) => f.toJSON());
                    context.commit('setCurrentFeatures', features);
                    return context.dispatch('cacheResultFeatures', features).then(() => features);
                });
            }
            return Promise.resolve();
        },
        fetchCurrentSimulation(context, { simulationId, refresh }) {
            if (context.state.currentSimulationState === ACTION_NONE || refresh && context.state.currentSimulationState === ACTION_REQUESTED) {
                context.commit('setCurrentSimulationState', ACTION_PENDING);
                context.commit('setCurrentSimulationStatistics', undefined);
                context.dispatch('fetchPhenomenons', { refresh: false }).then(() => {
                    return client.get('simulations', simulationId);
                }).then(data => {
                    const simulation = data.toJSON();
                    simulation.featureArcs = data.featureArcs.map(fA => fA.toJSON());
                    context.commit('setCurrentSimulation', simulation);
                    context.commit('setCurrentSimulationState', ACTION_REQUESTED);
                }).then(() => {
                    return context.dispatch('fetchCurrentCaseContext', {
                        caseContextId: context.state.currentSimulation.case_context_id,
                        refresh: false
                    });
                }).then(() => {
                    return context.dispatch('fetchCurrentSimulationStatistics', {
                        simulationId,
                        refresh: false
                    });
                }).catch(() => {
                    context.commit('addAlert', ['danger', "Couldn't get your simulation!"]);
                    context.commit('setCurrentSimulationState', ACTION_NONE);
                });
            }
            return Promise.resolve();
        },
        fetchCurrentSimulationStatistics(context, { simulationId, refresh }) {
            return client.get(`simulations/${simulationId}`, 'statistics').then(data => {
                context.commit('setCurrentSimulationStatistics', data.toJSONTree());
            }).catch(() => {
                context.commit('addAlert', ['danger', "Couldn't get your simulation statistics!"]);
            });
        },
        fetchCurrentCaseContext(context, { caseContextId, refresh }) {
            if (context.state.currentCaseContextState === ACTION_NONE || (
                    (refresh || context.state.currentCaseContext && context.state.currentCaseContext.id !== caseContextId) &&
                    context.state.currentCaseContextState === ACTION_REQUESTED
                )) {
                context.commit('setCurrentCaseContextState', ACTION_PENDING);
                return client.get('case-contexts', caseContextId).then(data => {
                    context.commit('setCurrentCaseContext', data.toJSONTree());
                    context.commit('setCurrentCaseContextState', ACTION_REQUESTED);
                }).catch(() => {
                    context.commit('addAlert', ['danger', "Couldn't get your case context!"]);
                    context.commit('setCurrentCaseContextState', ACTION_NONE);
                });
            }
            return Promise.resolve();
        },
        fetchFeatureArc(context, id) {
            var simulation = context.state.currentSimulation;
            if (simulation) {
                return axios.get(apiPrefix + '/simulations/' + simulation.id + '/feature-arcs/' + id)
                    .then((data) => {
                        context.commit('addFeatureArc', { simulation: simulation.id, arcId: id, arc: data.data.data });
                        return data.data;
                    });
            }
            return Promise.resolve();
        },
        fetchDataset(context, id) {
            var simulation = context.state.currentSimulation;
            if (simulation) {
                return axios.get(apiPrefix + '/simulations/' + simulation.id + '/features/' + id)
                    .then((data) => {
                        context.commit('addDataset', { simulation: simulation.id, dataset: data.data.data });
                        return data;
                    });
            }
            return Promise.resolve();
        },
        setRTL(context, val) {
            context.commit('SET_RTL', val.rtl);
        }
    },

});

store.watch(
    (state, getters) => [getters.timeOffset, state.currentSimulation],
    async() => {
        await store.dispatch('fetchResultFeatures');
        await store.dispatch('fetchCurrentFeatures');
        store.dispatch('fetchResultFeatures');
    }
);

store.watch(
    (state) => [state.currentSimulation],
    () => {
        store.dispatch('fetchCurrentInformationals');
    }
);

export default store;
export {
    store,
    ACTION_NONE,
    ACTION_PENDING,
    ACTION_REQUESTED
};