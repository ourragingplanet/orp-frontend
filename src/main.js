/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

import Vue from 'vue';

import VueLogger from 'vuejs-logger';
import GetTextPlugin from 'vue-gettext'
import { BootstrapVue, IconsPlugin, PaginationPlugin } from 'bootstrap-vue'
import 'mapbox-gl-leaflet';
import './sass/app.scss';
import './sass/leaflet.css';
import './assets/leaguegothic/stylesheet.css';
import './assets/css/main.css';
import './js/newsticker/jquery.newsTicker.js';
import mapboxgl from 'mapbox-gl';
import VueTour from 'vue-tour';
import VueMapbox from "vue-mapbox";

window.mapboxgl = mapboxgl;

//import Vuex from 'vuex';
import AsyncComputed from 'vue-async-computed';

import App from './js/App';
import router from './router/index.js';

import { library } from '@fortawesome/fontawesome-svg-core'
import { faUndo, faPlay, faStop } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faPlay)
library.add(faUndo)
library.add(faStop)

/* Vue.component(
 *    'passport-clients',
 *    require('./js/components/passport/Clients.vue')
 *);
 *
 *Vue.component(
 *    'passport-authorized-clients',
 *    require9'./js/components/passport/AuthorizedClients.vue')
 *);
 *
 *
 *Vue.component(
 *    'passport-personal-access-tokens',
 *    require('./js/components/passport/PersonalAccessTokens.vue')
 *);
 */
import axios from 'axios';
import { DeferredReady } from 'vue-leaflet/src/deferredReady';
import store from './js/state/store';
import i18n from './i18n'

import translations from './locales/translations.json'

Vue.prototype.$http = axios;

const isProduction = process.env.NODE_ENV === 'production';

const logOptions = {
    isEnabled: true,
    logLevel: isProduction ? 'error' : 'debug',
    stringifyArguments: false,
    showLogLevel: true,
    showMethodName: true,
    separator: '|',
    showConsoleColors: true
};

require('vue-tour/dist/vue-tour.css')

Vue.mixin({
    methods: {
        carbonToJs: function(time) {
            var secs = new Date(Date.parse(time));
            //var secs = new Date(Date.parse(time.substr(0, time.indexOf('.')).replace(' ', 'T')));
            return secs;
        }
    }
});
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(DeferredReady);
//Vue.use(Vuex);
Vue.use(VueTour);
Vue.use(AsyncComputed);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(PaginationPlugin);
Vue.use(VueLogger, logOptions);
Vue.use(VueMapbox, { mapboxgl: mapboxgl });
Vue.use(GetTextPlugin, {
    availableLanguages: {
        en_GB: 'English (UK+I)',
        /* ru: 'Russian', */
        ar_SA: 'Arabic',
    },
    defaultLanguage: 'en_GB',
    languageVmMixin: {
        computed: {
            currentKebabCase: function() {
                return this.current.toLowerCase().replace('_', '-')
            },
        },
    },
    translations: translations,
    silent: true,
});

Object.defineProperty(Vue.prototype, '$publicPath', {
    get() {
        return this.$root.publicPath;
    }
});

Object.defineProperty(Vue.prototype, '$bus', {
    get() {
        return this.$root.bus;
    }
});

Object.defineProperty(Vue.prototype, '$apiPrefix', {
    get() {
        return this.$root.apiPrefix;
    }
});

var bus = new Vue({
    i18n
});

// use beforeEach route guard to set the language
router.beforeEach((to, from, next) => {

    // use the language from the routing param or default language
    let lang_ = to.params.lang;
    if (!lang_) {
        lang_ = 'en_GB'
    }

    // set the current language for i18n.
    i18n.locale = lang_
    Vue.config.language = lang_

    store.dispatch('setLocale', lang_);

    //Check rtl regarding to language
    if ("ar" === lang_ || "ar_SA" === lang_)
        store.dispatch('setRTL', { rtl: true });
    else
        store.dispatch('setRTL', { rtl: false });

    next()
})

new Vue({
    el: '#app',
    i18n,
    router,
    template: '<App/>',
    components: { App },
    store,
    beforeCreate() { this.$store.commit('initialiseStore'); },
    data: {
        bus: bus,
        apiPrefix: process.env.VUE_APP_API_PREFIX,
        publicPath: process.env.VUE_APP_PUBLIC_PATH
    }
});