import Vue from 'vue';
import PanelPage from '@/js/components/PanelPage';
import Router from 'vue-router';
import HomePage from '@/js/components/HomePage';
import i18n from '../i18n.js'

Vue.use(Router);

export default new Router({
 mode: "history",
  routes: [
    {
      path: '/:lang/v',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: '',
          name: 'Home',
          component: HomePage
        },
        {
          path: 'panel',
          name: 'Panel',
          component: PanelPage
        },
        {
          path: 's/:simulationId',
          name: 'Simulation',
          component: PanelPage,
          props: true
        }
      ]
    }
  ]
});
