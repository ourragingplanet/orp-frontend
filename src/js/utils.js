function getObjectWithEnumerableGetter(key, callback) {
  class DynamicObject {
    get [key]() {
      return callback();
    }
  }
  const dynamicObject = new DynamicObject();
  const descriptor = Object.getOwnPropertyDescriptor(DynamicObject.prototype, key);
  descriptor.enumerable = true;
  Object.defineProperty(dynamicObject, key, descriptor);
  return dynamicObject;
}

export default {
  getObjectWithEnumerableGetter
};
