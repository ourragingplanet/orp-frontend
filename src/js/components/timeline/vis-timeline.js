
import vis from 'vis';
import Vue from 'vue';
<script src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.1/moment-with-locales.min.js"></script>
var VisTimeLine = Vue.extend({
  template: '<div id="visualizations"></div>',
  props: ['time'],

  ready: function () {

    var container = document.getElementById('visualizations');
    var items = new vis.DataSet([
      { id: 1, content: 'item 1', start: '2013-04-20' },
      { id: 2, content: 'item 2', start: '2013-04-14' },
      { id: 3, content: 'item 3', start: '2013-04-18' },
      { id: 4, content: 'item 4', start: '2013-04-16', end: '2013-04-19' },
      { id: 5, content: 'item 5', start: '2013-04-25' },
      { id: 6, content: 'item 6', start: '2013-04-27' }
    ]);

    var options = {
      height: '10px',
      // use the new locale
      locale: 'it'
    };
    var timeline = new vis.Timeline(container, items, options);
    timeline.moveToTime(this.time);
    this.$log.debug('t:' + this.time);
  }
});
Vue.component('times', VisTimeLine);
